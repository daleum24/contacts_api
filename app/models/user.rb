class User < ActiveRecord::Base
  attr_accessible :name, :email

  validates :name, :email, presence: true
  validates :email, uniqueness: true


  has_many(
    :contact_shares,
    class_name: "ContactShare",
    foreign_key: :user_id,
    primary_key: :id
  )

  has_many(
    :contacts,
    class_name: "Contact",
    foreign_key: :user_id,
    primary_key: :id
  )

  has_many(
    :groups,
    class_name: "Group",
    primary_key: :id,
    foreign_key: :user_id
  )

  has_many(
    :comments,
    class_name: "Comment",
    primary_key: :id,
    foreign_key: :user_id
  )



  has_many :shared_contacts, through: :contact_shares, source: :contact

  def self.favorite_contacts(id)
    User.find(id).contacts.where("contacts.favorite = ?",true)
  end
end
