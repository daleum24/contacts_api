class Contact < ActiveRecord::Base
  attr_accessible :name, :email, :user_id, :favorite

  validates :name, :email, :user_id, presence: true

  has_many(
    :contact_shares,
    class_name: "ContactShare",
    foreign_key: :contact_id,
    primary_key: :id
  )

  belongs_to(
    :user,
    class_name: "User",
    foreign_key: :user_id,
    primary_key: :id
  )

  has_many(
    :group_contacts,
    class_name: "GroupContact",
    primary_key: :id,
    foreign_key: :contact_id
  )

  has_many(
    :comments,
    class_name: "Comment",
    primary_key: :id,
    foreign_key: :contact_id
  )

  has_many :groups, through: :group_contacts, source: :group

  has_many :shared_users, through: :contact_shares, source: :user


  def self.contacts_for_user_id(user_id)
    query = <<-SQL
    SELECT c.*
    FROM contacts c
    LEFT OUTER JOIN contact_shares cs ON c.id = cs.contact_id
    WHERE c.user_id = ? OR cs.user_id = ?
    SQL

    Contact.find_by_sql([query, user_id, user_id])
  end

end
