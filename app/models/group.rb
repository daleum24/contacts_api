class Group < ActiveRecord::Base
  attr_accessible :name, :user_id

  belongs_to(
    :user,
    class_name: "User",
    primary_key: :id,
    foreign_key: :user_id
  )

  has_many(
    :group_contacts,
    class_name: "GroupContact",
    primary_key: :id,
    foreign_key: :group_id
  )

  has_many :contacts, through: :group_contacts, source: :contact

end
