class Comment < ActiveRecord::Base
  attr_accessible :user_id, :contact_id, :body

  belongs_to(
    :author,
    class_name: "User",
    primary_key: :id,
    foreign_key: :user_id
  )

  belongs_to(
    :subject,
    class_name: "Contact",
    primary_key: :id,
    foreign_key: :contact_id
  )

end
