class UsersController < ApplicationController
  def index
    render :json => User.all
  end

  def create
    puts "#{params.class}  >  #{params}  "
    user = User.new(params[:user])
    if user.save
      render :json => user
    else
      render :json => user.errors, :status => :unprocessable_entity
    end
  end

  def show
    #render :text => "I'm in the show action!"
    #render :json => params
    render :json => User.find(params[:id])
  end

  def destroy
    User.delete(params[:id])
    render :text => "User #{params[:id]} deleted!"
  end

  def update
    User.update(params[:id], params[:user])
    render :text => "User #{params[:id]} updated!"
  end

  def favorites
    render :json => User.favorite_contacts(params[:id])
  end
end
