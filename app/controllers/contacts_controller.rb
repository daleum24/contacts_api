class ContactsController < ApplicationController
  def index
    p params
    render :json => Contact.contacts_for_user_id(params[:user_id])
  end

  def show
    render :json => Contact.find(params[:id])
  end

  def update
    Contact.update(params[:id],params[:contact])
    render :json => Contact.find(params[:id])
  end

  def destroy
    Contact.delete(params[:id])
    head(:ok)
  end

  def create
    contact = Contact.new(params[:contact])
    if contact.save
      render :json => contact
    else
      render :json => contact.errors, :status => :unprocessable_entity
    end
  end


end
