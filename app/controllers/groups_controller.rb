class GroupsController < ApplicationController
  def index
    render :json => User.find(params[:user_id]).groups
  end

  def show
    render :json => Group.find(params[:id]).contacts
  end

  def destroy
    Group.delete(params[:id])
    head(:ok)
  end

  def create
    group = Group.new(params[:group])
    if group.save
      render :json => group
    else
      render :json => group.errors, :status => :unprocessable_entity
    end
  end

  def update
    Group.update(params[:id],params[:group])
    render :json => Group.find(params[:id])
  end

end
