class CommentsController < ApplicationController

  def index
    render :json => User.find(params[:user_id]).comments
  end

  def create
    comment = Comment.new(params[:comment])
    if comment.save
      render :json => comment
    else
      render :json => comment.errors, :status => :unprocessable_entity
    end
  end

  def update
    Comment.update(params[:id], params[:comment])
    render :text => "Comment #{params[:id]} updated!"
  end

  def show
    render :json => Comment.find(params[:id])
  end

  def destroy
    Comment.delete(params[:id])
    render :text => "Comment #{params[:id]} deleted!"
  end

end
