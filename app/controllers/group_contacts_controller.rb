class GroupContactsController < ApplicationController

  def create
    group_contact = GroupContact.new(params[:group_contact])

    if group_contact.save
      render :json => group_contact
    else
      render :json => group_contact.errors, :status => :unprocessable_entity
    end
  end

  def destroy
    GroupContact.delete(params[:id])
    head :ok
  end
end
