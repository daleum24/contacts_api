RouteProject::Application.routes.draw do

  resources :users do
    member do
      get :favorites
    end

    resources :contacts, :only => [:index, :create]

    resources :groups,   :only => [:index, :create]

    resources :comments, :only => [:index, :create]

  end

  resources :contacts, :except     => [:index, :create, :new, :edit]

  resources :groups, :except       => [:index, :create, :new, :edit]

  resources :comments, :except     => [:index, :create, :new, :edit]

  resources :contact_shares, :only => [:create, :destroy]

  resources :group_contacts, :only => [:create, :destroy]



  # get 'users' => 'users#index'
#   post 'users' => 'users#create'
#   get 'users/new' => 'users#new', :as => :new_user
#   get 'users/:id/edit' => 'users#edit', :as => :edit_user
#
#   get 'users/:id' => 'users#show', :as => :user
#   put 'users/:id' => 'users#update'
#
#   delete 'users/:id' => 'users#destroy'
#




end
