require "addressable/uri"
require "rest-client"


url = Addressable::URI.new(
  scheme: 'http',
  host: 'localhost',
  port: 3000,
  path: '/comments/1'
    #   query_values: {
    #   'some_category[a_key]' => 'another value',
    #   'some_category[a_second_key]' => 'yet another value',
    #   'some_category[inner_inner_hash][key]' => 'value',
    #   'something_else' => 'aaahhhhh'
    # }
).to_s

#params = {contact_share: {user_id: 1, contact_id: 3} }
#puts RestClient.post(url, params)
puts RestClient.delete(url)