# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


# User.create!([  {name: "adam", email: "adam@appacademy.io"},
#                 {name: "dale", email: "dale@appacademy.io"},
#                 {name: "john", email: "john@appacademy.io"}])

# Contact.create!([ {name: "tommy", email: "tommy@work", user_id: 1},
#                   {name: "sid", email: "sid@work", user_id: 2},
#                   {name: "anuj", email: "anuj@work", user_id: 1}])


# ContactShare.create!([ {user_id:1 , contact_id:1},
#                        {user_id:1 , contact_id:3},
#                        {user_id:2 , contact_id:2} ])

# Group.create!([ {name: "friends", user_id:1 },
#                 {name: "coworkers", user_id:2 }
# ])
#
# GroupContact.create!([  { group_id:1, contact_id:1 },
#                         { group_id:1, contact_id:2 },
#                         { group_id:2, contact_id:2 },
#                         { group_id:2, contact_id:3 }
#
# ])

Comment.create!([ {user_id: 1, contact_id: 1, body: "very helpful..."},
                  {user_id: 2, contact_id: 2, body: "not so much."}
])